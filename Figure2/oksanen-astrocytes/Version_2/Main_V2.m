%% Collect Linux\Slurm metadata
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Job is running on node:')
[~, node] = system('hostname');
disp(node)
disp('Job is run by user:')
[~, user] = system('whoami');
disp(user)
disp('Current slurm jobs of current user:')
[~, sq] = system(['squeue -u ', user]);
disp(sq)
tic
disp(['Start: ' datestr(now, 'yyyymmdd_HHMMSS')])
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')


%%
defineThreshold = false;
if defineThreshold
    ThresholdData = load('/mnt/IrisHCS/Data/PatrycjaMulica/VimentinS100B/PKM_20201027_gfap_s100b_20x_TripA53TGCAstro_20201027_150933_out/data.mat');
    ThresholdData = ThresholdData.Data;
    Q75_S100Vec = ThresholdData.Q75_S100;
    Q75_VimentinVec = ThresholdData.Q75_Vimentin;
    figure
    histogram(Q75_S100Vec, 1000, 'FaceColor', 'r')
    hold on
    histogram(Q75_VimentinVec, 1000,  'FaceColor', 'g')
    legend({'S100', 'Vimentin'})
end

ThresholdS100 = 250;
ThresholdVimentin = 250;

%% Flags

% delete(gcp('nocreate'))
% myCluster = parcluster('local');
% AvailableWorkers = myCluster.NumWorkers;
% if AvailableWorkers >= 28
%     pool = parpool(28)
% else
%     pool = parpool(1)
% end

addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))

if ~exist('InPath') % if Inpath is provided  via command line, use that one
    %InPath = '/work/projects/lcsb_hcs/Data/PatrycjaMulica/GFAPSF100beta/PKM_20200723_training_optimization_20200806_111823_in';
    %InPath = '/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Patrycja/PKM_20201027_gfap_s100b_20x_TripA53TGCAstro_20201027_150933/AssayPlate_PerkinElmer_CellCarrierUltra96';
    InPath = '/mnt/IrisHCS/Data/PatrycjaMulica/SP_markers_Test_astro20XRep1andRep2_20230705_154918_in';
end

MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);

if ~exist('OutPath') % if Outpath is provided  via command line, use that one
    %OutPath = '/work/projects/lcsb_hcs/Data/PatrycjaMulica/GFAPSF100beta/PKM_20200723_training_optimization_20200806_111823_out';
    %OutPath = '/mnt/D/tmp20200211';
    OutPath = '/mnt/IrisHCS/Data/PatrycjaMulica/SP_markers_Test_astro20XRep1andRep2_20230705_154918_out';
end

%% Prepare folders
mkdir(OutPath)
PreviewPath = [OutPath, filesep, 'Previews'];
mkdir(PreviewPath)

%% Log
f_LogDependenciesLinux(mfilename, OutPath)


%% Load Metadata
ObjectsAll = {};
%MetaData = f_CV8000_getChannelInfo(InPath, MesPath);
InfoTable = MetaData.InfoTable{:};
Wells = unique(InfoTable.Well);
fieldProgress = 0;
for w = 1:numel(Wells)
    WellThis = Wells{w};
    InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, WellThis),:);
    FieldsThisWell = unique(InfoTableThisWell.Field);
    for f = 1:numel(FieldsThisWell)
        fieldProgress = fieldProgress + 1;
        FieldThis = FieldsThisWell{f};
        InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, FieldsThisWell{f}),:);
        ChannelsThisField =  unique(InfoTableThisField.Channel);
        ImPaths = cell(1, numel(ChannelsThisField));
        for c = 1:numel(ChannelsThisField)
            ChannelThis = ChannelsThisField{c};
            InfoTableThisChannel = InfoTableThisField(strcmp(InfoTableThisField.Channel,ChannelThis),:);
            InfoTableThisChannel = sortrows(InfoTableThisChannel, 'Plane', 'ascend');
            chThisPaths = cell(numel(ChannelsThisField),1);
            for p = 1:height(InfoTableThisChannel)
                chThisPaths{p} = InfoTableThisChannel{p, 'file'}{:};
                %for t = 1:height()
            end
            ImPaths{c} = chThisPaths;
            MesFile = MetaData.MeasurementSettingFileName;
        end
       FieldMetaData{fieldProgress} = {ImPaths, MesFile, Wells{w}, FieldsThisWell{f}};
    end
end

disp('Debug point')
FieldMetaDataTable = cell2table(vertcat(FieldMetaData{:}))

for i = 1:numel(FieldMetaData)
%parfor i = 1:numel(FieldMetaData)
%parfor i = 1:2
    try
        i    
        ch1files = sort(FieldMetaData{i}{1}{1});
        ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
        ch1 = cat(3,ch1Collector{:}); % vol(ch1, 0, 2000) Hoechst
    
        ch2files = FieldMetaData{i}{1}{2};
        ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
        ch2 = cat(3,ch2Collector{:}); % vol(ch2, 0, 800) GFAP deep red
        
        ch3files = FieldMetaData{i}{1}{3};
        ch3Collector = cellfun(@(x) imread(x), ch3files, 'UniformOutput', false);
        ch3 = cat(3,ch3Collector{:}); % vol(ch3, 0, 2000) Nestin 488

        ch4files = FieldMetaData{i}{1}{4};
        ch4Collector = cellfun(@(x) imread(x), ch4files, 'UniformOutput', false);
        ch4 = cat(3,ch4Collector{:}); % vol(ch4, 0, 3000) CellMaskO
        
        MesFile = FieldMetaData{i}{2};
        WellThis = FieldMetaData{i}{3};
        FieldThis = FieldMetaData{i}{4};
        
        Objects = f_imageAnalysis(ch1, ch2, ch3, ch4, WellThis, FieldThis, MesFile, PreviewPath, ThresholdS100, ThresholdVimentin);
        
        ObjectsAll{i} = Objects;
    catch
       continue
    end
end

Data = vertcat(ObjectsAll{:});
save([OutPath, filesep, 'data.mat'], 'Data');
writetable(Data, [OutPath, filesep, 'data.csv'])
disp('Script completed successfully')
