function [Objects] = f_imageAnalysis(ch1B, ch2DR, ch3G, ch4O, WellThis, FieldThis, MesFile, PreviewPath, ThresholdS100, ThresholdVimentin)
%Summary is fine do previews and clean comments
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    Summary = table();

    %% segment nuclei
    chNuc = max(ch1B, [], 3); % imtool(chNuc, [])
    chNucMed = medfilt2(chNuc); % imtool(chNucMed, [])
    NucMask = chNucMed > 150; % imtool(NucMask, [])

    %% segment cells on CellMask
    chCellMask = max(ch4O, [], 3); % imtool(chCellMask, [])
    chCellMaskMed = medfilt2(chCellMask); % imtool(chCellMaskMed, [])
    CellMask = chCellMaskMed > 150; % imtool(CellMask, [])
 
    %% Features
    Objects = table();
    Objects.Well = {WellThis};
    Objects.Field = {FieldThis};
    Objects.NucArea = sum(NucMask(:));
    Objects.CellArea = sum(CellMask(:));
    Objects.meanDeepRedInCellMask = mean(ch2DR(CellMask));
    Objects.meanGreenInCellMask = mean(ch3G(CellMask));
    
    %% Previews

    imSize = size(chNuc);
    [BarMask, BarCenter] = f_barMask(100, 0.32393102760889064, imSize, imSize(1)-50, 50, 20);
    %it(BarMask)
    
    BPreview = f_imoverlayIris(imadjust(max(ch1B,[],3), [0 0.03], [0 1]), BarMask, [1 1 1]);%imtool(BPreview)
    DRPreview = f_imoverlayIris(imadjust(max(ch2DR,[],3), [0 0.03], [0 1]), BarMask, [1 1 1]);%imtool(DRPreview)
    GPreview = f_imoverlayIris(imadjust(max(ch3G,[],3), [0 0.09], [0 1]), BarMask, [1 1 1]);%imtool(GPreview)
    OPreview = f_imoverlayIris(imadjust(max(ch4O,[],3), [0 0.03], [0 1]), BarMask, [1 1 1]);%imtool(OPreview)
    
    NucPreview = f_imoverlayIris(imadjust(chNuc, [0 0.01], [0 1]), imdilate(bwperim(NucMask),strel('disk', 1)), [0 0 1]);
    NucPreview = f_imoverlayIris(NucPreview, BarMask, [1 1 1]);
    %imtool(NucPreview)
    
    CellMaskPreview = f_imoverlayIris(imadjust(chCellMask, [0 0.01], [0 1]), imdilate(bwperim(CellMask),strel('disk', 1)), [1 1 0]);
    CellMaskPreview = f_imoverlayIris(CellMaskPreview, BarMask, [1 1 1]);
    %imtool(CellMaskPreview)
    
    NucPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Nuc.png'];
    CellMaskPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_CellMask.png'];
    BPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_B.png'];
    DRPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_DR.png'];
    GPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_G.png'];
    OPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_O.png'];

    imwrite(NucPreview, NucPreviewPath)
    imwrite(CellMaskPreview, CellMaskPreviewPath)
    imwrite(BPreview, BPreviewPath)
    imwrite(DRPreview, DRPreviewPath)
    imwrite(GPreview, GPreviewPath)
    imwrite(OPreview, OPreviewPath)
    
end

