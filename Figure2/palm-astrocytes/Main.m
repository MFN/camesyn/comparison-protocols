%% Collect Linux\Slurm metadata
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Job is running on node:')
[~, node] = system('hostname');
disp(node)
disp('Job is run by user:')
[~, user] = system('whoami');
disp(user)
disp('Current slurm jobs of current user:')
[~, sq] = system(['squeue -u ', user]);
disp(sq)
tic
disp(['Start: ' datestr(now, 'yyyymmdd_HHMMSS')])
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

%% Flags

% delete(gcp('nocreate'))
% myCluster = parcluster('local');
% AvailableWorkers = myCluster.NumWorkers;
% if AvailableWorkers >= 28
%     pool = parpool(28)
% else
%     pool = parpool(1)
% end

%% define thresholds

defineThreshold = false
if defineThreshold
    Tdata = load('/mnt/IrisHCS/Data/PatrycjaMulica/GFAPTom20/PKM_20200805_gfapandtom20_optimization_20200805_155915_out_20201111/GFAPperNucdata.mat');
    Tdata = Tdata.GFAPperNucData;
    TVec = Tdata.QuantileGFAP_075;
    figure
    histogram(TVec, 500);
end

ThresholdGFAP = 500;
%%
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsIris'))

if ~exist('InPath') % if Inpath is provided  via command line, use that one
    InPath = '/mnt/IrisHCS/Data/PatrycjaMulica/AstroComparison/PKM_20210927_gfap_astro_PalmMorph_20211028_152159_in';
end

MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);

if ~exist('OutPath') % if Outpath is provided  via command line, use that one
    OutPath = '/mnt/IrisHCS/Data/PatrycjaMulica/AstroComparison/PKM_20210927_gfap_astro_PalmMorph_20211028_152159_out';
end

%% Prepare folders
mkdir(OutPath)
PreviewPath = [OutPath, filesep, 'Previews'];
mkdir(PreviewPath)

%% Log
f_LogDependenciesLinux(mfilename, OutPath)


%% Load Metadata
ObjectsAll = {};
ObjectsPerFieldAll = {};
ObjectsPerSomaAll = {};
%MetaData = f_CV8000_getChannelInfo(InPath, MesPath);
InfoTable = MetaData.InfoTable{:};
Wells = unique(InfoTable.Well);
fieldProgress = 0;
for w = 1:numel(Wells)
    WellThis = Wells{w};
    InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, WellThis),:);
    FieldsThisWell = unique(InfoTableThisWell.Field);
    for f = 1:numel(FieldsThisWell)
        fieldProgress = fieldProgress + 1;
        FieldThis = FieldsThisWell{f};
        InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, FieldsThisWell{f}),:);
        ChannelsThisField =  unique(InfoTableThisField.Channel);
        ImPaths = cell(1, numel(ChannelsThisField));
        for c = 1:numel(ChannelsThisField)
            ChannelThis = ChannelsThisField{c};
            InfoTableThisChannel = InfoTableThisField(strcmp(InfoTableThisField.Channel,ChannelThis),:);
            InfoTableThisChannel = sortrows(InfoTableThisChannel, 'Plane', 'ascend');
            chThisPaths = cell(numel(ChannelsThisField),1);
            for p = 1:height(InfoTableThisChannel)
                chThisPaths{p} = InfoTableThisChannel{p, 'file'}{:};
                %for t = 1:height()
            end
            ImPaths{c} = chThisPaths;
            MesFile = MetaData.MeasurementSettingFileName;
        end
       FieldMetaData{fieldProgress} = {ImPaths, MesFile, Wells{w}, FieldsThisWell{f}};
    end
end
list = cell2table(vertcat(FieldMetaData{:})); 
disp('Debug point')

HandleOverfitting = 0
if HandleOverfitting
    ch1filesLeft = sort(FieldMetaData{367}{1}{1});
    ch1CollectorLeft = cellfun(@(x) imread(x), ch1filesLeft, 'UniformOutput', false);
    ch1Left = cat(3,ch1CollectorLeft{:}); % vol(ch1Left, 0, 2000) Hoechst
    
    ch1filesRight = sort(FieldMetaData{370}{1}{1});
    ch1CollectorRight = cellfun(@(x) imread(x), ch1filesRight, 'UniformOutput', false);
    ch1Right = cat(3,ch1CollectorRight{:}); % vol(ch1Right, 0, 2000) Hoechst
    
    ch1 = [ch1Left, ch1Right]; % vol(ch1, 0, 2000)
    
    ch2filesLeft = sort(FieldMetaData{367}{1}{2});
    ch2CollectorLeft = cellfun(@(x) imread(x), ch2filesLeft, 'UniformOutput', false);
    ch2Left = cat(3,ch2CollectorLeft{:}); % vol(ch2Left, 0, 2000) Hoechst
    
    ch2filesRight = sort(FieldMetaData{370}{1}{2});
    ch2CollectorRight = cellfun(@(x) imread(x), ch2filesRight, 'UniformOutput', false);
    ch2Right = cat(3,ch2CollectorRight{:}); % vol(ch2Right, 0, 2000) Hoechst
    
    ch2 = [ch2Left, ch2Right]; % vol(ch2, 0, 2000)

    i = 370 % just to fill
    MesFile = FieldMetaData{i}{2};
    WellThis = FieldMetaData{i}{3};
    FieldThis = FieldMetaData{i}{4};
    
    [GFAPperNucObjects, ObjectsPerField] = f_imageAnalysis(ch1, ch2, WellThis, FieldThis, MesFile, PreviewPath, ThresholdGFAP);
    GFAPperNucObjectsAll{i} = GFAPperNucObjects;
    ObjectsPerFieldAll{i} = ObjectsPerField;
    ObjectsPerSomaAll{i} = ObjectsPerSoma;
    
    listIms = cell2table(FieldMetaData')

else
    for i = 1:numel(FieldMetaData)% i = 732
        try
            i    
            ch1files = sort(FieldMetaData{i}{1}{1});
            ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
            ch1 = cat(3,ch1Collector{:}); % vol(ch1, 0, 2000) Hoechst

            ch2files = FieldMetaData{i}{1}{2};
            ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
            ch2 = cat(3,ch2Collector{:}); % vol(ch2, 0, 2000) GFAP

            MesFile = FieldMetaData{i}{2};
            WellThis = FieldMetaData{i}{3};
            FieldThis = FieldMetaData{i}{4};

            [GFAPperNucObjects, ObjectsPerField] = f_imageAnalysis(ch1, ch2, WellThis, FieldThis, MesFile, PreviewPath, ThresholdGFAP);
            GFAPperNucObjectsAll{i} = GFAPperNucObjects;
            ObjectsPerFieldAll{i} = ObjectsPerField;
           
        catch ME
            disp(['Error for i ', num2str(i)])
            errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
                ME.stack(1).name, ME.stack(1).line, ME.message);
            fprintf(1, '%s\n', errorMessage);
            continue
        end
    end
end


%% QC: find ids with 7 columns %% added 20201105
GoodIDs = cellfun(@(x) size(x,2), GFAPperNucObjectsAll) == 8;
GFAPperNucObjectsAll_afterQC = GFAPperNucObjectsAll(GoodIDs);
GFAPperNucData = vertcat(GFAPperNucObjectsAll_afterQC{:});
save([OutPath, filesep, 'GFAPperNucdata.mat'], 'GFAPperNucData');
writetable(GFAPperNucData, [OutPath, filesep, 'GFAPperNucdata.csv'])

ObjectsPerFieldData = vertcat(ObjectsPerFieldAll{:});
save([OutPath, filesep, 'ObjectsPerFielddata.mat'], 'ObjectsPerFieldData');
writetable(ObjectsPerFieldData, [OutPath, filesep, 'ObjectsPerFielddata.csv'])

disp('Script completed successfully')
