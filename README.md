# comparison-protocols
This project corresponds to the paper: "A comparison of two iPSC differentiation protocols to generate human astrocyte cultures" (Mulica et al.)

## Folder `Figure2`

In this folder, the codes for analyzing astrocytic morphology are stored, both for Oksanen and Palm astrocytes. 

Subfolder `oksanen-astrocytes` contains the Matlab codes for analyzing the morphology of Oksanen astrocytes. The Matlab version used was: 2020a.

The corresponding raw data is stored at:
```
/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Patrycja/PKM_20210927_gfap_astro_OksanenMorph_20210927_205648/AssayPlate_PerkinElmer_CellCarrierUltra96
/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Patrycja/PKM_20210927_gfap_astro2_OksanenMorph2_20210927_222659/AssayPlate_PerkinElmer_CellCarrierUltra96
```

Subfolder `palm-astrocytes` contains the Matlab codes for analyzing the morphology of Palm astrocytes. The Matlab version used was: 2020a. The corresponding raw data is stored at:
```
/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Patrycja/PKM_20210927_gfap_astro_PalmMorph_20211028_152159/AssayPlate_PerkinElmer_CellCarrierUltra96
```

Subfolder `r-scripts` contain R scripts used to summarize the data. The graphs were generated using the summarized data in GraphPad. 

# Folder `Figure3, 4A, B5`

The analysis was performed in R.
Raw data is stored at `bds-02.uni.lux` server in directory `/mnt/data`

# Folder `Figure 4C, D, E and S3`

The analysis was performed in R.





